# Modèle de bannière pour podcast

Le fichier **bannière.xcf** est un modèle avec un rapport de taille de 3 pour 1 pour la création d'une bannière au format de Gimp.
Soit 4500 pixels de largeur x 1500 pixels de hauteur. C'est la taille minimal requise pour une bannière.

## Modèle de couverture pour podcast

Le fichier **Cover.xcf** est un carré de 1400 pixels soit la taille minimale requise. 

## Configuration recommandée 

Pour un résultat propre et scalable il est fortement recommandé d'utiliser des images au format `svg` celui-ci permet de garder la qualité en agrandissement ou rétrécissement de l'image. Les piles de calques ont également leurs importances pour des effets de profondeurs.

## Contribution

Si vous désirez contribuer libre à vous de suggéré des améliorations ou de nouvelles fonctionnalités. Amis graphistes ou simples utilisateurs expérimentées de Gimps a vos claviers 😉 ⌨️

